# xml-to-json

Use Node 16

## Getting started

transforma y obtiene la información de archivos XML a Json 

Los archivos XML tienen una estructura definida en su v3 y v4, se procesa y se obtienen los datos, la salida se formatea y se almaena en un arhcivo csv 

Los XML que se pueden procesar son facturas recibidas en México en las versiones 3 y 4

## Test and Deploy

1. Descarga el proyecto
1. Tener una factura en versión 3 o 4
1. Configura el script correspondiente la ruta del archivo para (parse-xml.js) o la ruta de la carpeta para (parse-folder.js)  
1. Ejecuta el script correspondiente para un archivo (parse-xml.js) o para una carpeta (parse-folder.js)
   ```
    node parse-xml.js
    node parse-folder.js
   ```  

1. El archivo **output.csv** es un ejemplo de la salida de los scripts, el cual se puede importar un excel para una mejor lectura 

## Contributing
¡Las contribuciones son bienvenidas! Si deseas mejorar alguna imagen de Docker o agregar nuevas imágenes, sigue estos pasos:

Haz un fork de este repositorio.
Realiza tus cambios en tu fork.
Envía un pull request a este repositorio.


## Authors and acknowledgment
Este proyecto fue creado y es mantenido por Aquiles Lázaro.

- Nombre: Aquiles Lázaro
- Correo Electrónico: aquileslazaroh@gmail.com
- Perfil de GitLab: [GitLab](https://gitlab.com/aquileslh)
- Perfil de GitHub: [GitHub](https://github.com/aquileslh)

## License
Este proyecto está bajo la [Licencia MIT](LICENSE).

La Licencia MIT es una licencia de código abierto muy permisiva que permite a otros utilizar, modificar y redistribuir tu código, ya sea en proyectos comerciales o no comerciales, siempre que se incluya el aviso de copyright y la licencia original.

Puedes encontrar más detalles en el archivo [LICENSE](LICENSE) o en la [página de la Licencia MIT](https://opensource.org/licenses/MIT).
