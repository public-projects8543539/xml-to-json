const fs = require('fs');
const xml2js = require('xml2js');

//Read the XML file
const filePath = '../Agosto/1aa03f99-c35cca731eaf.xml';
const outputPath = './output.csv';

const outputStream = fs.createWriteStream(outputPath);

var tax16 = "";
var tax0 = "";

// Function to read and process a single XML file
function processXmlFile(filePath, outputStream) {
  const xmlData = fs.readFileSync(filePath, 'utf8');

  const parser = new xml2js.Parser({ explicitArray: false });

  parser.parseString(xmlData, (err, result) => {
    if (err) {
      console.error(`Error parsing XML file "${filePath}":`, err);
    } else {
      processXmlData(result, outputStream, filePath);
    }
  });
}

// Function to process parsed XML data
function processXmlData(result, outputStream, filePath) {
  const jsonData = JSON.stringify(result, null, 2);
  // Print the JSON data to the console
  console.log('JSON Data:\n', jsonData);
  const csvTitle = "Version; Fecha; TipoDeComprobante; LugarExpedicion; Moneda; EmisorNombre; EmisorRFC; Nombre; RFC; DomicilioFiscal; RegimenFiscal; UsoCFDI; FormaPago; MetodoPago; CondicionesDePago; Subtotal; Total; Descuento; ClaveUnidad; Unidad; Descripcion; Cantidad; ValorUnitario; Importe; Descuento; ObjetoImp; Impuesto; TipoFactor; TasaOCuota; Base; Importe; TotalImpuestosTrasladados; base/0;Impuesto/0;TipoFactor/0;TasaOCuota/0;Importe/0; base/16;Impuesto/16;TipoFactor/16;TasaOCuota/16;Importe/16";
  outputStream.write(csvTitle + '\n');

  try {
    const transferred = result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Traslados"]["cfdi:Traslado"];

    if (Array.isArray(transferred)) {
      // Handle when concepto is an array
      transferred.forEach((item, index) => {
        tax(item);
      });
    } else if (typeof transferred === 'object') {
      console.log("objeto de transferencia ")
      tax(transferred);
    }
  } catch (error) {
    console.log(`Invalid type for "cfdi:Traslado", Error processing XML file:`, error);
  }

  try {
    const taxes = result["cfdi:Comprobante"]["cfdi:Impuesto"];

    if (result["cfdi:Comprobante"]["cfdi:Impuestos"]) {
      const transferred = result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Traslados"]["cfdi:Traslado"];
      tax0 = '0;0;0;0;0';
      tax16 = '0;0;0;0;0';
      if (Array.isArray(transferred)) {
        // Handle when concepto is an array
        transferred.forEach((item, index) => {
          tax(item);
        });
      } else if (typeof transferred === 'object') {
        console.log("objeto de transferencia ")
        tax(transferred);
      }
    } else {
      tax0 = '0;0;0;0;0';
      tax16 = '0;0;0;0;0';
    }

  } catch (error) {
    console.log(`Invalid type for "cfdi:Traslado", Error processing XML file:`, error);
    process.exit(1);
  }

  try {
    const version = result["cfdi:Comprobante"]["$"]["Version"];
    const date = result["cfdi:Comprobante"]["$"]["Fecha"];
    const wayToPay = result["cfdi:Comprobante"]["$"]["FormaPago"];
    const metodOfPay = result["cfdi:Comprobante"]["$"]["MetodoPago"];
    const paymentConditions = result["cfdi:Comprobante"]["$"]["CondicionesDePago"];
    const subTotal = result["cfdi:Comprobante"]["$"]["SubTotal"].replace('.', ',');
    const total = result["cfdi:Comprobante"]["$"]["Total"].replace('.', ',');
    const discount = result["cfdi:Comprobante"]["$"]["Descuento"] ? result["cfdi:Comprobante"]["$"]["Descuento"].replace('.', ',') : 0;
    const currency = result["cfdi:Comprobante"]["$"]["Moneda"];
    const typeOfReceipt = result["cfdi:Comprobante"]["$"]["TipoDeComprobante"];
    const expeditionPlace = result["cfdi:Comprobante"]["$"]["LugarExpedicion"];

    const transmitter = result["cfdi:Comprobante"]["cfdi:Emisor"]["$"]["Nombre"];
    const transmitterRfc = result["cfdi:Comprobante"]["cfdi:Emisor"]["$"]["Rfc"];

    const receiver = result["cfdi:Comprobante"]["cfdi:Receptor"]["$"]["Nombre"];
    const receiverRfc = result["cfdi:Comprobante"]["cfdi:Receptor"]["$"]["Rfc"];
    const taxAddressRecipient = result["cfdi:Comprobante"]["cfdi:Receptor"]["$"]["DomicilioFiscalReceptor"];
    const taxRegime = result["cfdi:Comprobante"]["cfdi:Receptor"]["$"]["RegimenFiscalReceptor"];
    const useCfdi = result["cfdi:Comprobante"]["cfdi:Receptor"]["$"]["UsoCFDI"];


    const totalTax = result["cfdi:Comprobante"]["cfdi:Impuestos"] ? result["cfdi:Comprobante"]["cfdi:Impuestos"]["$"] ? result["cfdi:Comprobante"]["cfdi:Impuestos"]["$"]["TotalImpuestosTrasladados"].replace('.', ',') : 0 : 0;

    console.log(version, date, transmitter, transmitterRfc,
      receiver, receiverRfc, taxAddressRecipient, taxRegime,
      useCfdi, wayToPay, paymentConditions, subTotal, total,
      discount, typeOfReceipt, expeditionPlace, currency, totalTax);

    const csvVoucher = `${version};${date};${typeOfReceipt};${expeditionPlace};${currency};${transmitter};${transmitterRfc};${receiver};${receiverRfc};${taxAddressRecipient};${taxRegime};${useCfdi};${wayToPay};${metodOfPay};${paymentConditions};${subTotal};${total};${discount}; ; ; ; ; ; ; ; ; ; ; ; ; ; ${totalTax}; ${tax0}; ${tax16}`;
    outputStream.write(csvVoucher + '\n');
  } catch (error) {
    console.log(`Invalid type for "cfdi:Comprobante", Error processing XML file:`, error);
    process.exit(1);
  }

  try {

    const concepto = result["cfdi:Comprobante"]["cfdi:Conceptos"]["cfdi:Concepto"];

    if (Array.isArray(concepto)) {
      // Handle when concepto is an array
      concepto.forEach((item, index) => {
        conceptos(item);
      });
    } else if (typeof concepto === 'object') {
      conceptos(concepto)
    } else {
      console.error(`Invalid type for "cfdi:Concepto" in XML file "${filePath}"`);
    }

  } catch (error) {
    console.error(`Invalid type for "cfdi:Concepto", Error processing XML file:`, error);
    process.exit(1);
  }

}

function conceptos(item) {
  const driveKey = item["$"]["ClaveUnidad"];
  const unit = item["$"]["Unidad"];
  const description = item["$"]["Descripcion"];
  const quantity = item["$"]["Cantidad"];
  const price = item["$"]["ValorUnitario"].replace('.', ',');
  const amount = item["$"]["Importe"].replace('.', ',');
  const discount = item["$"]["Descuento"] ? item["$"]["Descuento"].replace('.', ',') : 0;
  const imposedObject = item["$"]["ObjetoImp"];

  const tax = item["cfdi:Impuestos"] ? item["cfdi:Impuestos"]["cfdi:Traslados"]["cfdi:Traslado"]["$"]["Impuesto"] : 0;
  const typeFactor = item["cfdi:Impuestos"] ? item["cfdi:Impuestos"]["cfdi:Traslados"]["cfdi:Traslado"]["$"]["TipoFactor"] : 0;
  const rateOrFee = item["cfdi:Impuestos"] ? item["cfdi:Impuestos"]["cfdi:Traslados"]["cfdi:Traslado"]["$"]["TasaOCuota"] : 0;
  const base = item["cfdi:Impuestos"] ? item["cfdi:Impuestos"]["cfdi:Traslados"]["cfdi:Traslado"]["$"]["Base"].replace('.', ',') : 0;
  const taxImport = item["cfdi:Impuestos"] ? item["cfdi:Impuestos"]["cfdi:Traslados"]["cfdi:Traslado"]["$"]["Importe"] ? item["cfdi:Impuestos"]["cfdi:Traslados"]["cfdi:Traslado"]["$"]["Importe"].replace('.', ',') : 0 : 0;

  console.log(driveKey, unit, description, quantity, price, amount, discount, imposedObject, tax, typeFactor, rateOrFee, base, taxImport);
  console.log("-------------------------");

  const csvConcept = `; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;${driveKey};${unit};${description};${quantity};${price};${amount};${discount};${imposedObject};${tax};${typeFactor};${rateOrFee};${base};${taxImport}`
  outputStream.write(csvConcept + '\n');
}

function tax(item) {
  if (item["$"]["TasaOCuota"] == "0.000000") {
    console.log("0%")
    const base = item["$"]["Base"] ? item["$"]["Base"].replace('.', ',') : 0;
    const tax = item["$"]["Impuesto"];
    const typeFactor = item["$"]["TipoFactor"];
    const rateOrFee = item["$"]["TasaOCuota"];
    const taxImport = item["$"]["Importe"].replace('.', ',');
    tax0 = `${typeFactor};${tax};${rateOrFee};${base};${taxImport}`;
    console.log(tax0);
  } else if (item["$"]["TasaOCuota"] == "0.160000") {
    console.log("16%")
    const base = item["$"]["Base"] ? item["$"]["Base"].replace('.', ',') : 0;
    const tax = item["$"]["Impuesto"];
    const typeFactor = item["$"]["TipoFactor"];
    const rateOrFee = item["$"]["TasaOCuota"];
    const taxImport = item["$"]["Importe"].replace('.', ',');
    tax16 = `${typeFactor};${tax};${rateOrFee};${base};${taxImport}`;
    console.log(tax16);
  }
}

processXmlFile(filePath, outputStream);